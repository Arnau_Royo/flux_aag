/*
* Dependències
*/
var gulp = require('gulp'),
  autoprefixer = require('gulp-autoprefixer'),
  minimage = require('gulp-tinypng');

/*
* Configuració de la tasca 'autoprefixar'
*/
gulp.task('autoprefixar', function () {
	gulp.src('./html/css/*.css')
	.pipe(autoprefixer('last 2 version'))
	.pipe(gulp.dest('./html/css/result/'));
});


/*
* Configuració de la tasca 'optimg'
*/
gulp.task('optimg', function () {
	gulp.src('./html/img/*.png')
	.pipe(minimage('rlRTYum4eYLceZ6VpEciPgqWgqdAU5Vn'))
	.pipe(gulp.dest('./html/img/result/'));
});


/*
* Configuració de la tasca 'comprovagit'
*/
